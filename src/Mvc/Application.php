<?php

namespace Mvc;

use Mvc\ModuleManager\ModuleManager;

class Application{

	/**
     * @var array
     */
    protected $configuration = null;

	/**
     * Constructor
     *
     * @param mixed $configuration
     */
    public function __construct($configuration)
    {
        $this->configuration  = $configuration;
    }

	public static function init($configuration = array())
    {
    	$modulesConfig = isset($configuration) ? $configuration : array();
    	$moduleManager = new ModuleManager($modulesConfig);
    	$moduleManager->loadModules();
        return new Application($configuration);
    }

    public function run()
    {
        $urlServer    = $_SERVER['SERVER_NAME'];  // imprime:www.site.com.br
        $urlEndereco  = $_SERVER ['REQUEST_URI']; // imprime: /contato
        $urlEndereco  = end(explode("/projetos/mvcPureBlog/", $urlEndereco));
        $params       = explode("/", $urlEndereco);

        $module     = $params[0];
        $controller = $params[1];
        $action     = $params[2];

        //verificar se module/controller/action existe
            //verificar eventos
            //executar eventos
        //instanciar e executar rota
    }
}