<?php

namespace Mvc\Config;

class ApplicationConfig
{
	protected $configuration;
	protected $pathConfig;
	protected $modules;
	protected $configModulePath;
	protected $configModuleFilePath;
	
	public function __construct($configPath){
		$this->configuration = require $configPath;
		$this->setPath($configPath);
		$this->setModules($this->configuration['modules']);
		$this->setConfigModulePath($this->configuration['module_listener_options']['module_path']);
		$this->setConfigModuleFilePath($this->configuration['module_listener_options']['config_module_path']);
	}

	public function setPath($configPath){
		$this->pathConfig = $configPath;
	}

	public function getPath(){
		return $this->pathConfig;
	}

	public function setModules($modules){
		if (is_array($modules) || $modules instanceof Traversable) {
            $this->modules = $modules;
        } else {
            throw new Exception\InvalidArgumentException(sprintf(
                'Parameter to %s\'s %s method must be an array or implement the Traversable interface',
                __CLASS__, __METHOD__
            ));
        }
	}

	public function getModules(){
		return $this->modules;
	}

	public function setConfigModulePath($configModulePath){
            $this->configModulePath = $configModulePath;
	}

	public function getConfigModulePath(){
		return $this->configModulePath;
	}

	public function setConfigModuleFilePath($configModuleFilePath){
            $this->configModuleFilePath = $configModuleFilePath;
	}

	public function getConfigModuleFilePath(){
		return $this->configModuleFilePath;
	}
}