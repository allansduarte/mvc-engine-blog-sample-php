# MVC Engine Blog Sample #

Este pacote é uma aplicação de blog simples, utilizando o padrão MVC + psr-1 + psr-2 + psr-3 + psr-4 + psr-7. Está aplicação é destinada para fins de aprendizagem.

### Descrição ###

* MVC baseado no Zend Framework 2
* Este MVC modular é configurável
* O core MVC é totalmente desacoplado, localizado em "src/Mvc"

### Dependências ###

* Composer.phar

### Configuração ###

* Utilizado o composer para PSR-4
* Necessário configurar o arquivo config/application.config.php
* Necessário configurar arquivos de configurações de módulo locais config.module.php