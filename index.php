<?php

require 'vendor/autoload.php';

define('BASE_PATH', dirname(realpath(__FILE__)) . '/');//constante necessária para carregar módulos

// Run the application!
Mvc\Application::init(require 'config/application.config.php')->run();